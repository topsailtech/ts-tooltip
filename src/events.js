export { fireEvent as fireEvent };

// listen for events with target.addEventListener(type, listener[, useCapture])
//   returns false if the event was cancelled by an event listener (Event.preventDefault())
function fireEvent(target_element, event_name, event_detail){
  var ev = new CustomEvent(event_name, {detail: event_detail, bubbles: true, cancelable: true});
  return target_element.dispatchEvent(ev);
};