export default `
<style>
  :host {
    display: inline-block;
    position: absolute;
    padding: 7px 0;
    visibility: hidden;
    z-index: 99;
  }
  :host(.hovering), :host(:hover) { visibility: visible; }

  :host .tip-inner {
    font-size: small;
    border: 1px solid #e8e8e8;
    border-radius: 5px;
  }

  :host .tip-content { padding: 8px; }

  :host .arrow {
    width: 0;
    height: 0;
    border-width: 0 8px 0 8px;
    border-style: solid dashed none;
    position: absolute;
    border-left-color: transparent;
    border-right-color: transparent;
  }

  :host(.bottom) .arrow { border-bottom-width: 8px; border-style: none dashed solid; }
  :host(.bottom) .arrow-front { top: 2px; }
  :host(.bottom) .arrow-back { top: 1px; }

  :host(:not(.bottom)) .arrow { border-top-width: 8px; border-style: solid dashed none; }
  :host(:not(.bottom)) .arrow-front { bottom: 2px; }
  :host(:not(.bottom)) .arrow-back { bottom: 1px; }

  :host(.left) .arrow { right: 16px; }
  :host(:not(.left)) .arrow { left: 16px; }

  /* DEFAULT - Skin : white */
  :host .tip-inner { background: #fff; color: #888; }
  :host .arrow-front { border-top-color: #fff; border-bottom-color: #fff; }
  :host .arrow-back { border-top-color: #e8e8e8; border-bottom-color: #e8e8e8; }

    /* Tips - Skin : black */
  :host(.black) .tip-inner   { background: #000; color: #fff; }
  :host(.black) .arrow-front { border-top-color: #000; border-bottom-color: #000; }
  :host(.black) .arrow-back  { border-top-color: #000; border-bottom-color: #000; }
</style>

<div class="tip-inner">
  <div class="tip-content">
    <div class="tip-msg">
      <slot></slot>
    </div>
  </div>
</div>
<div class="arrow arrow-back"></div>
<div class="arrow arrow-front"></div>
`
