export { absoluteOffset as absoluteOffset };

// Offset to first positioned (not static) ancestor element
function absoluteOffset(elem) {
  var offset = null,
      run_el = elem;
  if ( run_el ) {
      offset = {left: 0, top: 0};
      do {
          offset.top += run_el.offsetTop;
          offset.left += run_el.offsetLeft;
          run_el = run_el.offsetParent;
      } while ( run_el && window.getComputedStyle(run_el).getPropertyValue("position") == 'static');
  }
  return offset;
};
