# \<ts-tooltip>

A Custom Element to provide hover-text, just like the @title attribute on most HTML elements. The difference is that the ts-tooltip can include an HTML fragment, instead of just text.

The hover text can either be inlined, or it can be retrieved via an AJAX call. This AJAX call will be done only on-demand, if the hover is being triggered. You can also force that hover text to be re-loaded freshly each time the hover bubble gets triggered.


## Usage

Wrap the text that should be hoverable into some html element.

Add right afterwards (as the following dom sibling):
```html
<script type="importmap">
  {
    "imports": {
      "ts-tooltip": "https://unpkg.com/@topsail/ts-tooltip"
    }
  }
</script>
<script type="module">
  import 'ts-tooltip';
</script>

<ts-tooltip>
  all my <b>hover</b> help HTML goes here.
</ts-tooltip>
```

or

```html
  <ts-tooltip href="url_to_retrieve_the_text_on_hover"></ts-tooltip>
```

### Options
* `delay` - (default "0") how many milliseconds to wait for the hover bubble to show up when hovering
* `href`  - (optional) URL to retrieve the hover bubble text
* `always-reload` - (optional) if present, re-fetch the bubble text from the server each time the text gets hovered


## Tooling configs

For most of the tools, the configuration is in the `package.json` to minimize the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.


## Local Demo with `web-dev-server`

```bash
npm install
npm start
```
To run a local development server that serves the basic demo located in `demo/index.html`


## Deploy new version
```bash
npm login
npm run deploy
```